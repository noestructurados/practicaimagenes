# Practica Imagenes - Clasificador de tipos de basuras
Realizado por: Valeria Capurro, Sofía Olalla Fernández de Soto y  Marta Simón Pinacho.

## 1.-Objetivo

El objetivo del proyecto es identificar los tipos de basura a partir de un dataset de imagenes, mediante la utilización de una red convolucional, primero creada desde cero y luego utilizando una ya preentrenada. 

## 2.-Modelo con  12 clases y sin balanceo de clases

El dataset original cuenta con 12 clases de basura: papel, carton, plastico, metal, basura generica, baterias, zapatos, ropa, vidrio verde, vidrio marron, vidrio blanco y basura organica. En total se cuenta con aproximadamente 15500 imagenes en total, del cual se dividio en train (70%), validacion (20%) y test (10%) originalmente. Teniamos un claro problema de desbalanceo de clases al tener casi 5 veces más imágenes de ropa que de cualquier otra clase tanto en train, como en validación y test.

Entrenamos una red convolucional haciendo data augmentation para todas las clases por igual. Esta se encuentra en el notebook **CNN_Keras.ipynb** en el **Apartado 2**.

## 3.-Data augmentation

Con el objetivo de balancear las clases, realizamos data augmentation solo en las clases minoritarias (todas menos "clothes"), e intentamos equiparar la cantidad de imágenes en todas las clases tanto para el conjunto de train como para el de validación; alzando el numero de imágenes casi a 50000 en total. Las proporciones se intentaron mantener pero al final obtuvimos una división de 80% train, validación 14% y test 6%. 

Estas operaciones se encuentran en el notebook **CNN_Keras.ipynb** en el **Apartado 3**.

## 4.-Modelo con  12 clases y con balanceo de clases

Luego de haber realizado data augmentation a 11 clases entrenamos la misma red, sin embargo vemos que los resultados no mejoran significativamente.

Este modelo se encuentra en el notebook **CNN_Keras.ipynb** en el **Apartado 4**.

## 5.- Modelo de 5 categorías.

Una vez entrenado el modelo con las doce clases y viendo los resultados, se tomó la decisión de entrenar el mismo modelo, con algulna modificación, unicamente con 5 clases, de tal manera que los resultados mejoraran. 

El notebook donde se puede encontrar este modelo es el de **CCN_Keras_5_categories.ipynb**

Los resultados obtenidos, tal y como se pueden ver en ese notebook previamente indicado, no fueron los deseados por lo que decidio intentar simplificar aún más el modelo. 

## 6.- Modelo de 2 categorías.

Después de ver los resultados obtenidos en el modelo de las 5 categorías y viendo que no eran nada buenos, se decidió realizar un nuevo modelo con solamente dos clases de salida, para ver si asi el modelo mejoraba algo a la hora de predecir el tipo de basura que aparecía en la imagen que recibía como output. 

Este nuevo modelo se encuentra en el notebook con el nombre de **CNN_Keras_2_categorias.ipynb**. 

Viendo que los resultados obtenidos, aun habiendo cambiado diferentes parametros no eran nada buenos se tomo la decisión de utilizar una red preentrenada. 

## 7.- Extracción manual de features

Para ver por qué podíamos estar teniendo tantos problemas a la hora de clasificar las imágenes, hicimos una extracción manual de features con el algoritmo SIFT, para tratar de ver qué features eran las que más le llamaban la atención a este algoritmo en cada una de las diferentes clases.

Lo que vimos fue que, tanto los fondos de las imagenes, como los logos, inscripciones, letras y dibujos de las diferentes clases (sobre todo en la de paper) estaban distrayendo mucho al algoritmo del objeto como tal, y estaba extrayendo features que no le servían de mucho para aprender a clasificar las 12 clases.

Esta extracción de features manual puede encontrarse en el notebook **FeatureExtractionManual.ipynb**

## 8.- Fine tuning de un modelo preentrenado con el dataset original

El siguiente paso que probamos fue realizar un fine tunning de un modelo preentrenado llamado Xception.

La arquitectura de red Xception se basa en la idea de que las operaciones de convolución en una red neuronal convolucional (CNN) pueden ser factorizadas en dos etapas separadas: una convolución en el espacio (espacial) y otra en el canal (depthwise). Esta separación de las operaciones de convolución permite que la red tenga menos parámetros lo que, a su vez, mejora la eficiencia computacional y reduce el sobreajuste.

Con esta red preentrenada y con el dataset original, al realizar fine tuning conseguimos unos resultados muy buenos. Conseguimos reconocer todas las clases con suficiente precision y conseguimos una matriz de confusion bastante buena. Además, el AUC roza el 100% en el conjunto de datos de test.

Todo el proceso de fine tunning puede encontrarse en el notebook **FineTunning_Original.ipynb**.

Para realizar una última prueba, se nos ocurrió que podíamos tratar de hacer fine tuning sobre el modelo anterior pero con el dataset balanceado, para tratar de ver si mejoraban o no los resultados.

## 9.- Fine tuning de un modelo preentrenado con data augmentation

En esta ultima prueba, realizamos fine tuning añadiendo las mismas capas y los mismos parámetros a la red que en el modelo anterior, pero esta vez lo entrenamos y validamos con el dataset balanceado.

No conseguimos mejorar los resultados del modelo anterior, de hecho, empeoraron bastante. Pensamos que ha podido ser porque el modelo está muy sobreentrenado. Adenás, al tener ya la red preentrenada suficiente conocimiento sobre las imagenes de entrada, al reentrenarlo con imagenes distorsionadas lo que hemos hecho ha sido confundir al modelo, haciendo que los resultados no sean nada buenos.

El proceso de fine tuning con el dataset balanceado se puede ver en el notebook **FineTunning_Augmentation.ipynb**.

# 10.- Conclusiones

Como hemos visto a lo largo de todas las pruebas, el modelo que mejores resultados ha dado ha sido el explicado en el apartado 8. Una red Xception de Keras preentrenada, a la que realizando fine tunning hemos conseguido mejorar hasta llegar prácticamente a la perfección en las predicciones del conjunto de datos de test.